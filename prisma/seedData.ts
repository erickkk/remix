import { PrismaClient } from "@prisma/client";
const db = new PrismaClient();

async function seed() {
  await Promise.all(
    getUser().map((user) => {
      return db.user.create({ data: user });
    })
  );
}

seed();

function getUser() {
  return [
    {
      name: "Eric Kwong",
    }
  ];
}