import { json, useLoaderData } from "remix";
import type { LoaderFunction } from "remix";
import type { User } from "@prisma/client";

import { db } from "~/utils/db.server";

type LoaderData = { users: Array<User> };
export const loader: LoaderFunction = async () => {
  const data: LoaderData = {
    users: await db.user.findMany(),
  };
  return json(data);
};


export default function Index() {
  const data = useLoaderData<LoaderData>();
  return (
    <div>
      <ul className="bg-black text-white	px-4 text-3xl font-bold underline">
        {data.users.map((user) => (
          <li key={user.id}>{user.name}</li>
        ))}
      </ul>
    </div>
  );

}
