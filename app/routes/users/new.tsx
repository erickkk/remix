import { ActionFunction, json, useFetcher } from "remix";
import type { LoaderFunction } from "remix";
import type { User } from "@prisma/client";
import { faker } from '@faker-js/faker';

import { db } from "~/utils/db.server";
import { useEffect } from "react";
import Button from "~/components/Button";

type LoaderData = { users: Array<User> };

export const loader: LoaderFunction = async () => {
  const data: LoaderData = {
    users: await db.user.findMany(),
  };
  return json(data);
};

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  try {
    const user = await db.user.create({ data: { name: body.get('name') } });
    return json({ user });
  } catch (error: any) {
    return json({ error: error.message });
  }
}


export default function Index() {
  const fetcher = useFetcher<LoaderData>();
  const createNewUser = () => {
    fetcher.submit({
      name: faker.name.findName()
    }, { method: "post", action: "/users/new", replace: false })
  }

  useEffect(() => {
    if (fetcher.type === "init") {
      fetcher.load("/users/new");
    }
  }, [fetcher]);


  return (
    <div className="p-4">
      <div className="border border-solid border-gray-100">
        {fetcher.data?.users.map((user) => (
          <div className="px-4 border-b-2 border-gray-500 border-solid" key={user.id}>{user.name}</div>
        ))}
      </div>
      <fetcher.Form>
        <div className="pt-2">
          <Button onClick={createNewUser} text="Generate New User" />
        </div>
      </fetcher.Form>
    </div>
  );

}
