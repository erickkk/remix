import { HTMLAttributes, FC } from 'react';
import classnames from 'classnames';

interface Props extends HTMLAttributes<HTMLButtonElement> {
    text: string;
}

const Button: FC<Props> = ({ onClick, text }) => {
    return (
        <button className={classnames(
            "bg-blue-500 rounded-lg p-1",
            "lg:text-white",
            "md:text-blue-900"
        )} onClick={onClick}>{text}</button>
    );
}

export default Button